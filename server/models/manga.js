var mongoose = require('mongoose');

var chapterSchema = mongoose.Schema({
  no: { type: Number, required: true },
  imgs: [ String ]
})
var mangaSchema = mongoose.Schema({
  name: { type: String, required: true },
  chapters: [ chapterSchema ]
}, {
  collection: 'manga'
});

module.exports = mongoose.model('Manga', mangaSchema);
