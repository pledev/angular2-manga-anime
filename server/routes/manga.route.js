var express = require('express'),
    router = express.Router();
var Manga = require('../models/manga');

router.get('/', function(req, res) {
  Manga.find().exec(function(error, mangas) {
    if(error) {
      res.status(500).send(error);
    } else {
      mangas.forEach(function(manga) {
        manga.chapters.sort(function(chapterA, chapterB) {
          return chapterA.no - chapterB.no;
        });
      });
      res.json({
        data: mangas
      });
    }
  });
});

module.exports = router;
