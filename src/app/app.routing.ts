import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import AppComponent from './app.component';
import MangaComponent from '../manga/manga.component'

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/manga',
    pathMatch: 'full'
  },
  {
    path: 'manga',
    component: MangaComponent
  }
]

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
