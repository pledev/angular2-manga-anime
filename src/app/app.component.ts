import { Component, OnInit } from '@angular/core';
import AuthService from '../auth/auth.service';

import '../../public/css/styles.css';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})

export default class AppComponent {
  isDarkTheme: boolean = false;
  
  constructor(private authService: AuthService) {}
}
