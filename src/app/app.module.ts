import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AUTH_PROVIDERS } from 'angular2-jwt';

import { MdSidenavModule } from '@angular2-material/sidenav';
import { MdToolbarModule } from '@angular2-material/toolbar';
import { MdButtonModule } from '@angular2-material/button';
import { MdIconModule } from '@angular2-material/icon';
import { MdInputModule } from '@angular2-material/input';

import { routing } from './app.routing';

import AppComponent from './app.component';
import MangaComponent from '../manga/manga.component';

import AuthService from '../auth/auth.service';

import { LazyLoadImageModule } from 'ng2-lazyload-image';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    LazyLoadImageModule ,
    MdSidenavModule.forRoot(),
    MdToolbarModule.forRoot(),
    MdButtonModule.forRoot(),
    MdIconModule.forRoot(),
    MdInputModule.forRoot(),
    routing
  ],
  declarations: [
    AppComponent,
    MangaComponent
  ],
  providers: [
    AuthService,
    AUTH_PROVIDERS
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
