import { Directive, ViewContainerRef, ComponentRef } from '@angular/core';

@Directive({
  selector: '[dialog]'
})

export default class DialogDirective {
  constructor(private viewContainerRef: ViewContainerRef) {}
}
