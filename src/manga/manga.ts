export class Chapter {
  no: number;
  imgs: String[]
}
export default class Manga {
  _id: String;
  title: String;
  chapters: Chapter[]
}
