import { Component, OnInit, Renderer } from '@angular/core';

import Manga from './manga';
import { Chapter } from './manga';
import MangaService from './manga.service';

@Component({
  selector: 'manga-list',
  templateUrl: './manga.component.html',
  styleUrls: [ './manga.component.css' ],
  providers: [ MangaService ]
})

export default class MangaComponent implements OnInit {
  private mangas: Manga[];
  private currentManga: Manga;
  private currentChapter: Chapter;
  private gotoChapterNo: number;
  private lastChapterNo: number;
  private errorMessage: string;

  constructor(private mangaService: MangaService, private renderer: Renderer) {}

  getMangas(): void {
    this.mangaService.getMangas().subscribe(
      (mangas: Manga[]) => {
        this.mangas = mangas;
        this.currentManga = mangas[0];
        this.currentChapter = this.currentManga.chapters[0];
        this.lastChapterNo = this.currentManga.chapters[this.currentManga.chapters.length - 1].no;
      },
      (error: any) => this.errorMessage = error
    )
  }
  nextChapter(): void {
    let currentChapterIndex = this.currentManga.chapters.indexOf(this.currentChapter);
    this.currentChapter = this.currentManga.chapters[currentChapterIndex + 1];
  }

  prevChapter(): void {
    let currentChapterIndex = this.currentManga.chapters.indexOf(this.currentChapter);
    this.currentChapter = this.currentManga.chapters[currentChapterIndex - 1];
  }

  gotoChapter(): void {
    if(!this.gotoChapterNo) {
      return;
    }
    if(this.gotoChapterNo < 1 || this.gotoChapterNo > this.lastChapterNo) {
      this.errorMessage = "This Chapter doesnt exist";
    }
    this.currentChapter = this.currentManga.chapters.filter(chapter => chapter.no == this.gotoChapterNo)[0];
    this.gotoChapterNo = undefined;
  }

  ngOnInit(): void {
    this.renderer.listenGlobal('window', 'scroll', (e: any) => {
      console.log('scroll')
    });
    this.getMangas();
  }
}
