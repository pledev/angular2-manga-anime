import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';
import 'angular2-jwt';
import 'ng2-lazyload-image';

import '@angular2-material/core';
import '@angular2-material/sidenav';

import 'rxjs';
